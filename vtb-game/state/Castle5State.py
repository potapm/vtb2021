from model.NewsDto import NewsDto
from state.State import State
from state.StatusState import StatusState


class Castle5State(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Ужасные морозы внезапно обрушились на ваши земли. Люди и кони умирают от холода, а доспехи воинов бьются словно стекло. Фениксы и Маги впали в спячку.",
            [
                {
                    "id": 1,
                    "text": "Заработать на замок",
                    "url": "https://apps.apple.com/ru/app/id1364026756"
                },
                {
                    "id": 2,
                    "text": "Оформить ипотеку",
                    "url": "https://www.vtb.ru/personal/ipoteka"
                },
                {
                    "id": 3,
                    "text": "Скитаться"
                }
            ],
            {
                "fire": 0,
                "earth": 0,
                "water": 0,
                "metal": 0,
                "wood": 0,
                "gold": 0
            },
            {
                "fire": 0,
                "earth": 0,
                "water": 0,
                "metal": 0,
                "wood": 0,
                "gold": 10000
            },
            1,
            10
        )

    def handle(self, request):
        from state.GenderState import GenderState
        self.context.set(GenderState(self.context))
        return self.context.get().response
