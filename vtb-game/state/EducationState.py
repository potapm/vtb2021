from model.NewsDto import NewsDto
from state.Castle1State import Castle1State
from state.State import State


class EducationState(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Крылатый дракон решил посетить ваши угодья и разрушил добрую половину ваших земель. Теперь нужно найти материал для восстановления производств.",
            [],
            {
                "fire": 10,
                "earth": 0,
                "water": -5,
                "metal": -5,
                "wood": 0,
                "gold": 0
            },
            {
                "fire": 0,
                "earth": 5,
                "water": 0,
                "metal": 0,
                "wood": -5,
                "gold": 0
            },
            15,
            1
        )

    def handle(self, request):
        from state.Castle5State import Castle5State
        self.context.set(Castle5State(self.context))
        return self.context.get().response
