import abc


class State(metaclass=abc.ABCMeta):
    def __init__(self, context):
        self.context = context
        self.response = None

    """
    Define an interface for encapsulating the behavior associated with a
    particular state of the Context.
    """

    @abc.abstractmethod
    def handle(self, request):
        pass
