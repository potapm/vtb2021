from model.NewsDto import NewsDto
from state.EducationState import EducationState
from state.State import State


class AgeState(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Вдруг на ваши земли налетели страшные болезни и вам пришлось вводить карантин. В результате все сидят по домам и ничего не делают.",
            [],
            {
                "fire": 0,
                "earth": -5,
                "water": 0,
                "metal": -5,
                "wood": 10,
                "gold": 0
            },
            {
                "fire": 5,
                "earth": 0,
                "water": -5,
                "metal": 0,
                "wood": 0,
                "gold": 0
            },
            10,
            1
        )

    def handle(self, request):
        self.context.set(EducationState(self.context))

        return self.context.get().response
