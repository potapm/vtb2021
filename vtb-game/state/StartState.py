from state.GenderState import GenderState
from state.State import State


class StartState(State):
    def __init__(self, context):
        super().__init__(context)

    """
    Implement a behavior associated with a state of the Context.
    """

    def handle(self, request):
        self.context.set(GenderState(self.context))

        return self.context.get().response
