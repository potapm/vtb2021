from model.NewsDto import NewsDto
from state.Castle5State import Castle5State
from state.State import State
from state.StatusState import StatusState


class Castle4State(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Ваше превосходительство! У меня печальная новость. Пока вы были в походе, воздушные демоны унесли замок. Мы остались на улице.\n\nПора позаботиться о приобретении нового жилья!",
            [],
            {
                "fire": 10,
                "earth": 0,
                "water": -5,
                "metal": -5,
                "wood": 0,
                "gold": 0
            },
            {
                "fire": 0,
                "earth": 1,
                "water": -1,
                "metal": 0,
                "wood": 0,
                "gold": 0
            },
            3,
            1
        )

    def handle(self, request):
        self.context.set(Castle5State(self.context))
        return self.context.get().response
