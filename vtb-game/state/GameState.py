import random

from model.NewsDto import NewsDto
from service.MLService import component as ml_service
from state.State import State


class GameState(State):
    def __init__(self, context):
        super().__init__(context)
        vector = [random.randint(-25, 25),
                  random.randint(-25, 25),
                  random.randint(-25, 25),
                  random.randint(-25, 25),
                  random.randint(-25, 25),
                  ]
        self.response = NewsDto(
            ml_service.generate_text(vector),
            [
                {
                    "id": 1,
                    "text": "Торговать"
                },
                {
                    "id": 2,
                    "text": "Ждать"
                }
            ],
            {
                "fire": vector[0] - vector[2] / 2 - vector[3] / 2,
                "earth": vector[1] - vector[3] / 2 - vector[4] / 2,
                "metal": vector[2] - vector[0] / 2 - vector[4] / 2,
                "water": vector[3] - vector[0] / 2 - vector[1] / 2,
                "wood": vector[4] - vector[1] / 2 - vector[2] / 2,
                "gold": 0
            },
            {
                "fire": vector[4] / 10 - vector[1] / 10,
                "earth": vector[0] / 10 - vector[2] / 10,
                "metal": vector[1] / 10 - vector[3] / 10,
                "water": vector[2] / 10 - vector[4] / 10,
                "wood": vector[3] / 10 - vector[0] / 10,
                "gold": 0
            },
            5,
            5
        )

    def handle(self, request):
        if self.context.step <= 100:
            self.context.set(GameState(self.context))
        self.context.step = self.context.step + 1

        return self.context.get().response
