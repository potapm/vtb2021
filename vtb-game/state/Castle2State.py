from model.NewsDto import NewsDto
from state.Castle3State import Castle3State
from state.State import State
from state.StatusState import StatusState


class Castle2State(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "В вашем королевстве появились разбойники и периодически грабят ваших жителей, торговцев и ваши склады.",
            [],
            {
                "fire": 10,
                "earth": 0,
                "water": -10,
                "metal": 10,
                "wood": -10,
                "gold": 0
            },
            {
                "fire": 2,
                "earth": 0,
                "water": -2,
                "metal": 3,
                "wood": -1,
                "gold": 0
            },
            3,
            1
        )

    def handle(self, request):
        from state.Castle5State import Castle5State
        self.context.set(Castle5State(self.context))
        return self.context.get().response
