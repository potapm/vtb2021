from model.NewsDto import NewsDto
from state.GameState import GameState
from state.State import State


class StatusState(State):

    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Откуда столько золота? Возможно ваши предки были истребителями драконов…",
            [
                {
                    "id": 1,
                    "text": "Да, это мой"
                },
                {
                    "id": 2,
                    "text": "Другой замок"
                }
            ],
            {
                "fire": 0,
                "earth": 0,
                "water": 0,
                "metal": 0,
                "wood": 0,
                "gold": 0
            },
            {
                "fire": 0,
                "earth": 0,
                "water": 0,
                "metal": 0,
                "wood": 0,
                "gold": 10000
            },
            1,
            0
        )

    def handle(self, request):
        if request == 1:
            self.context.set(GameState(self.context))
        else:
            self.context.set(StatusState(self.context))

        return self.context.get().response
