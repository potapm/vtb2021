class Context:
    """
    Define the interface of interest to clients.
    Maintain an instance of a ConcreteState subclass that defines the
    current state.
    """

    def __init__(self):
        self._state = None
        self.step = 0

    def set(self, state):
        self._state = state

    def get(self):
        return self._state

    def request(self, request):
        return self._state.handle(request)
