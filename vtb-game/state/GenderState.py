from model.NewsDto import NewsDto
from state.AgeState import AgeState
from state.State import State


class GenderState(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Однажды вам удалось посетить гору с гнездами зеленых драконов. Это были лучшие дни вашей жизни.",
            [
                {
                    "id": 1,
                    "text": "Отдохну"
                },
                {
                    "id": 2,
                    "text": "Оседлаю дракона"
                },
                {
                    "id": 3,
                    "text": "Убью всех драконов"
                }
            ],
            {
                "fire": 0,
                "earth": 0,
                "water": 0,
                "metal": 0,
                "wood": 0,
                "gold": 0
            },
            {
                "fire": 0,
                "earth": 0,
                "water": 0,
                "metal": 0,
                "wood": 0,
                "gold": 0
            },
            0,
            1
        )

    def handle(self, request):
        if request == 3:
            from state.Castle3State import Castle3State
            self.context.set(Castle3State(self.context))
        elif request == 2:
            from state.Castle1State import Castle1State
            self.context.set(Castle1State(self.context))
        else:
            self.context.set(AgeState(self.context))

        return self.context.get().response
