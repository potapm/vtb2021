from model.NewsDto import NewsDto
from state.Castle4State import Castle4State
from state.State import State
from state.StatusState import StatusState


class Castle3State(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Внезапно к вам приехали ваши родственники и вы так отметили это событие что теперь вам предстоит долго восстанавливать провиант",
            [],
            {
                "fire": 0,
                "earth": 10,
                "water": 10,
                "metal": 0,
                "wood": -5,
                "gold": 0
            },
            {
                "fire": 0,
                "earth": 2,
                "water": 2,
                "metal": 0,
                "wood": -1,
                "gold": 2000
            },
            0,
            1
        )

    def handle(self, request):
        self.context.set(Castle4State(self.context))

        return self.context.get().response
