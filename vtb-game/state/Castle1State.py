from model.NewsDto import NewsDto
from state.Castle2State import Castle2State
from state.State import State
from state.StatusState import StatusState


class Castle1State(State):
    def __init__(self, context):
        super().__init__(context)
        self.response = NewsDto(
            "Многочисленные пожары и оползни обрушились на ваши земли, однако гильдия ремесленников соорудила металлические укрепления на подходах к вашим деревням чтобы избежать тотальной катастрофы.",
            [],
            {
                "fire": 0,
                "earth": -15,
                "water": 0,
                "metal": 20,
                "wood": -5,
                "gold": 0
            },
            {
                "fire": 0,
                "earth": 0,
                "water": -5,
                "metal": 5,
                "wood": -5,
                "gold": 100
            },
            20,
            1
        )

    def handle(self, request):
        self.context.set(Castle2State(self.context))
        return self.context.get().response
