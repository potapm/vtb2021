# Echo route
import json

from flask import Blueprint, request

from service.StateService import component as state_service
from state.Context import Context
from state.StartState import StartState

news = Blueprint('news', __name__)


@news.route('/news', methods=['POST'])
def echo():
    return state_service.post(json.loads(request.data)['id']).toJSON()


class StateService:
    def __init__(self):
        self.context = Context()
        self.context.set(StartState(self.context))

    def reset(self):
        self.context.set(StartState(self.context))

    def post(self, request):
        self.context.request(request)


component = StateService()
