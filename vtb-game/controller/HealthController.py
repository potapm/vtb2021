# Echo route
from flask import Blueprint

health = Blueprint('health', __name__)


@health.route('/')
def hello_world():  # put application's code here
    return 'VTB World!'


@health.route('/health', methods=['GET'])
def echo():
    return 'alive'
