from flask import Flask

from controller.HealthController import health
from controller.NewsController import news

HOST = '127.0.0.1'
PORT = 5000

app = Flask(__name__)
app.register_blueprint(health)
app.register_blueprint(news)

if __name__ == '__main__':
    app.run(host=HOST, port=PORT)
