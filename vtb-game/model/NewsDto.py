import json


class NewsDto:
    def __init__(self, text, actions, in_moment, deltas, duration, delay):
        self.text = text
        self.actions = actions
        self.in_moment = in_moment
        self.deltas = deltas
        self.duration = duration
        self.delay = delay

    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__,
            sort_keys=True, indent=4)
