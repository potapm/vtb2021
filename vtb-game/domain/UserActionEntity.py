from sqlalchemy import Column, String, Integer, DateTime
from sqlalchemy.sql import func

from domain.Base import Base


class UserActionEntity(Base):
    __tablename__ = 'user_actions'

    id = Column(Integer, primary_key=True)
    device_uuid = Column('device_uuid', String(32))
    action = Column('action', String(256))
    description = Column('description', String(64))

    time_created = Column(DateTime(timezone=True), server_default=func.now())
    time_updated = Column(DateTime(timezone=True), onupdate=func.now())

    def __init__(self, device_uuid, action, description):
        self.device_uuid = device_uuid
        self.action = action
        self.description = description
