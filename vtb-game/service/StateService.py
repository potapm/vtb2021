import logging
import typing

from state.Context import Context
from state.StartState import StartState

logging.basicConfig(format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


class StateService:

    # context: typing.Union[Context]

    def __init__(self):
        self.context = Context()
        self.context.set(StartState(self.context))

    def reset(self):
        self.context.set(StartState(self.context))

    def post(self, request):
        return self.context.request(request)


component = StateService()
