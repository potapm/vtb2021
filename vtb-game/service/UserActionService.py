import json
import logging

from domain import AlchemyEncoder
from repository import *

logging.basicConfig(format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


# Service for sending file to model and saving result
class UserActionService:
    def __init__(self):
        pass

    @staticmethod
    def get_user_actions():
        result = []
        for lecture in user_action_repository.get_all():
            result.append(json.loads(json.dumps(lecture, cls=AlchemyEncoder)))

        return json.dumps(result)

    @staticmethod
    def get_user_action(id):
        return json.dumps(user_action_repository.get(id), cls=AlchemyEncoder)


component = UserActionService()
